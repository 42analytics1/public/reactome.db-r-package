CREATE TABLE pathway2gene (
  DB_ID VARCHAR(10) NOT NULL,                  -- Reactome DB_ID
  gene_id VARCHAR(10) NOT NULL                  -- Entrez Gene
);
CREATE TABLE reactome2go(
  DB_ID VARCHAR(10) NOT NULL,
  go_id VARCHAR(10)
);
CREATE TABLE map_counts (
  map_name VARCHAR(80) PRIMARY KEY,
  count INTEGER NOT NULL
);
CREATE TABLE map_metadata (
  map_name VARCHAR(80) NOT NULL,
  source_name VARCHAR(80) NOT NULL,
  source_url VARCHAR(255) NOT NULL,
  source_date VARCHAR(20) NOT NULL
);
CREATE TABLE metadata (
  name VARCHAR(80) PRIMARY KEY,
  value VARCHAR(255)
);
CREATE TABLE pathway2name (
  DB_ID VARCHAR(10) NOT NULL,              -- Reactome DB_ID
  path_name VARCHAR(150) NOT NULL         -- Reactome pathway name
);
CREATE INDEX Ipathway2gene ON pathway2gene(gene_id);