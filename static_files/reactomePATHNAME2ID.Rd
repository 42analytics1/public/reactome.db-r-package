\name{reactomePATHNAME2ID}
\alias{reactomePATHNAME2ID}
\title{An annotation data object that maps Reactome pathway names to
  identifiers for the corresponding pathway names used by Reactome}
\description{
   reactomePATHNAME2ID maps Reactome pathway names to pathway identifiers used by
   Reactome for various pathways
}
\details{
  This is an R object containing key and value pairs. Keys are
  Reactome pathway names and values are pathway identifiers. Values are vectors of
  length 1.

  Mappings were based on data provided by: Reactome
  http://reactome.org/download/current/ReactomePathways.txt
  With a date stamp from the source that is the same as the time stamp of the package.

}
\references{
\url{http://www.reactome.org/}
}
\examples{
	xx <- as.list(reactomePATHNAME2ID)
	if(length(xx) > 0){
		# get the value for the first key
		xx[[1]]
		# Get the values for a few keys
		if(length(xx) >= 3){
			xx[1:3]
		}
	}
}
\keyword{datasets}
