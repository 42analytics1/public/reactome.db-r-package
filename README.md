
# reactome.db R package

This repository contains code to generate the reactome.db R package.

## Badges

[![License: CC BY 4.0](https://img.shields.io/badge/License-CC_BY_4.0-lightgrey.svg)](https://creativecommons.org/licenses/by/4.0/)

## Authors

- [@wligtenberg](https://gitlab.com/wligtenberg)


## Run Locally

Clone the project

```bash
  git clone https://gitlab.com/42analytics1/public/reactome.db-r-package
```

Go to the project directory

```bash
  cd reactome.db-r-package
```

Install dependencies

```R
  install.packages(c("data.table", "DBI", "fs", "jinjar", "RSQLite", "R.utils"))
```

Run code

```bash
  Rscript ./generate_reactome_package.R
```


## Support

For support, email willem@42analytics.eu.


## Acknowledgements

 - [Reactome](https://reactome.org/)
 - [Bioconductor](https://www.bioconductor.org/)
 - [Marc Carlson](https://www.linkedin.com/in/marc-carlson-b52aab2/)

